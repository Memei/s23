// console.log("hello");

/*
	Objects
		- is a data type that is used to represent real world objects. It is also a connection of related data and/or functionalities.

	Creating objects using object literal:
		Syntax: 
			let objectName = {
				keyA: valueA,
				keyB: valueB,
			}
*/

let student = {
	firstName: "Mae",
	lastName: "Sargento",
	age: 26,
	studentId: "2022-001212",
	email: ["rms@mail.com", "mira@mail.com"],
	address: {
		street: "123 Ilang-Ilang St.",
		city: "Quezon City",
		country: "Philippines"
	}
}

console.log("Result from creating an object:");
console.log(student);
console.log(typeof student);

// Creating Objects using Constructor Function
/*
	created a reusable function to create several objects that have the same data structure. this is useful for creating multiple instances/copies of an object.

	Syntax:
		function objectName(valueA, valueB) {
			this.keyA = valueA
			this.keyB = valueB
		}

		let variable = new function objectName(valueA, valueB);
		console.log(variable);


		"this" is a keyword that is used for invoking; it referes to the global object
		- don't forget to add 'new' keyword when we are creating the variables.
*/

// We use Pascal Casing for the ObjectName when creating objects using constructor function.

function Laptop(name, manufactureDate) {
	this.name = name
	this.manufactureDate = manufactureDate
}

	let laptop = new Laptop("Lenovo", 2008);
	console.log("Result of creating objects using object constructor: ")
	console.log(laptop);

	let myLaptop = new Laptop("MacBook Air", [2020, 2021])
	console.log("Result of creating objects using object constructor: ")
	console.log(myLaptop);

	let oldLaptop = Laptop("Portal R2E CCMC", 1980)
	console.log("Result of creating objects using object constructor: ")
	console.log(oldLaptop);


// Creating empty objects as placeholders
let computer = {};
let myComputer = new Object()
console.log(computer);
console.log(myComputer);

myComputer = {
	name: "Asus",
	manufactureDate: 2012
}

console.log(myComputer);

console.log("");
//Mini Activity

/*	function myCar(brand, model, manufactureDate) {
		this.brand = brand
		this.model = model
		this.manufactureDate = manufactureDate
	}

	let car = new myCar("Tesla", "Model S", 2021)
	console.log("My new car is:")
	console.log(car)

	let otherCar = new myCar("Rolls Royce", "Spectre", 2023)
	console.log("My other car is:")
	console.log(otherCar)

	console.log("");
*/
// Accessing Object Property

/*
	Using the dot notation

	Syntax:
		objectName.propertyName
*/

console.log("Result from dot notation: " + myLaptop.name)

/*
	Using the bracket notation

	Syntax:
			objectName["name"]
*/

console.log("Result from bracket notation: " + myLaptop["name"]);

// Accessing array objects

let array = [laptop, myLaptop];
// let array = [{name: "Lenovo", manufactureDate: 2008}, {name: "MacBook Air", manufactureDate: [2019, 2020]}];

// Dot Notation
console.log(array[0].name)

// Square bracket nontation
console.log(array[0]["name"])

// Initializing / Adding / Deleting / Reassigning Object Properties

let car = {}
console.log(car)

// Adding object Properties
car.name = "Honda Civic"
console.log("Result from adding property using dot notation: ")
console.log(car);


car["manufacture date"] = 2019
console.log(car)

// Deleting object properties
delete car["manufacture date"]

// car["manufacture date"] = " "
console.log("result from deleting object properties:")
console.log(car)

// Reassign object properties
car.name = "Tesla"
console.log("result from re assigning object property:")
console.log(car)

console.log("");

/*
	Object Method
		- a method where a function serves as a value in a property. They are also functions and one of the key differences they have is that methods are function related to a specific object property.
*/

let person = {
	name: "John",
	talk: function() {
		console.log("Hello! My name is " + this.name);
	}
}
console.log(person)
console.log("Result from object methods:");
person.talk()

person.walk = function() {
	console.log(this.name + " walked 25 steps forward.")
}

person.walk()

let friend = {
	firstName: "John",
	lastName: "Doe",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["johnD@mail.com", "joe12@yahoo.com"],
	introduce: function () {
		console.log("Hello! My name is " + this.firstName + " " + this.lastName)
	}
}

friend.introduce()

// Real World Application

/*
	Scenario:
		1. We would like to create a game that would have several polermon to interact with each other.
		2. every pokemon would have the same sets of stats, properties and functions.

*/

// Using Object Literals
/*let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This pokemon tackled targetPokemon")
		console.log("targetPokemon's health is now reduces to targetPokemonHealth")
	},
	faint: function() {
		console.log("Pokemon fainted")
	}
}

console.log(myPokemon);

// Using Object Constructor



function Pokemon(name, level) {

	// properties
	this.name = name
	this.level = level
	this.health = 3 * level
	this.attack = 2 * level

	// Methods
	this.tackle =  function(target) {
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced " + (target.health - this.attack))
	},
	this.faint = function () {
		console.log(this.name + " fainted")
	}
}

let charizard = new Pokemon("Charizard", 12);
let squirtle = new Pokemon("Squirtle", 6);
console.log(charizard);
console.log(squirtle);

charizard.tackle(squirtle);

console.log("");
*/
//MAIN ACTIVITY:

// WDC028v1.5b-23 | JavaScript - Objects
// Graded Activity:

// Part 1: 
//     1. Initialize/add the following trainer object properties:
//       Name (String)
//       Age (Number)
//       Pokemon (Array)
//       Friends (Object with Array values for properties)
//     2. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
//     3. Access the trainer object properties using dot and square bracket notation.
//     4. Invoke/call the trainer talk object method.

//Code Here:

let Trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Balbasaur"],
	friend: {
		friends: ["Brock", "Misty"],
		hoenn: ["May", "Max"],
	},
	talk: function() {
		console.log("Pikachu! I choose you!");
	}
}
console.log(Trainer)
console.log("Result of dot notation:");
console.log(Trainer.name);
console.log("Result of square bracket notation:");
console.log(Trainer['pokemon']);
console.log("Result of talk method:");
Trainer.talk();

console.log("");


// 1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
// 	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
// 	(target.health - this.attack)

// 2.) If health is less than or equal to 5, invoke faint function


//Code Here:



function Pokemon(name, level) {

	this.name = name
	this.level = level
	this.health = 2 * level
	this.attack = level

	
	this.tackle =  function(target) {
	this.faint = function() {
		console.log(target.name + " fainted")
	}
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced " + (target.health - this.attack))
	if ((target.health - this.attack) <=5) {
		this.faint();
	} 
	target.health = target.health - this.attack;
	console.log(target);
	}
}

let Pikachu = new Pokemon("Pikachu", 12);
let Geodude = new Pokemon("Geodude", 8);
let Mewtwo = new Pokemon("Mewtwo", 100);
console.log(Pikachu);
console.log(Geodude);
console.log(Mewtwo);

Geodude.tackle(Pikachu);
Geodude.tackle(Pikachu);
Geodude.tackle(Pikachu);

Mewtwo.tackle(Geodude);

